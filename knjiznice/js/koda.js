
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';


var username = "ois.seminar";
var password = "ois4fri";
var curentUser;

 ///// MOCKUP DATA///////
 var emptyUser = {
  ehrId:"unknown",
  name: "",
  surname: "",
  age: "unknown",
  eyeColor: "unknown",
  dob:"unknown",
  addres:"unknown",
  gender:"UNKNOWN",
  height:"unknown",
  weight:"unknown",
  pressure:"Not yet mesured",
  bmi:"Not yet mesured",
  weightChanges:{
    "2009":0,
    "2010":0,
    "2011":0,
    "2012":0,
    "2013":0,
    "2014":0,
    "2015":0,
    "2016":0,
    "2017":0,
    "2018":0,
    "2019":0,
  },
  alergies:{

  },
  medicine:{

  }
};

 var Ragnar = {
  ehrId:"",
  name: "Ragnar",
  surname: "Lodbrok",
  age: "unknown",
  eyeColor: "Blue",
  dob:"1000-05-18T00:00:00.000Z",
  addres:"The North",
  gender:"MALE",
  height:176,
  weight:3,
  pressure:"Not yet mesured",
  bmi:"nan",
  weightChanges:{
    "2009":6,
    "2010":6,
    "2011":6,
    "2012":5,
    "2013":5,
    "2014":5,
    "2015":5,
    "2016":5,
    "2017":5,
    "2018":3,
    "2019":3,
  },
  alergies:{
    alergy1:"water",
    alergy4:"air",
    alergy5:"polen",
  },
  medicine:{
    med1:"Cyclosporine",
    med2:"Sandimmune",
    med6:"Dovonex",
    med7:"Restasis",
  }
};

var Arya = {
  ehrId:"",
  name: "Arya",
  surname: "Stark",
  age: "13",
  eyeColor: "Brown",
  dob:"0155-12-08T00:00:00.000Z",
  addres:"The North",
  gender:"FEMALE",
  height:165,
  weight:68,
  pressure:"Not yet mesured",
  bmi:"nan",
  weightChanges:{
    "2009":45,
    "2010":47,
    "2011":59,
    "2012":65,
    "2013":68,
    "2014":72,
    "2015":65,
    "2016":68,
    "2017":63,
    "2018":66,
    "2019":68,
  },
  alergies:{
    alergy3:"mars",
  },
  medicine:{
    med1:"Cyclosporine",
    med2:"Sandimmune",
    med3:"Neoral",
    med4:"Taclonex",
    med5:"Calcipotriene topical",
    med6:"Dovonex",
    med7:"Restasis",
  }
};

var Drogon = {
  ehrId:"",
  name: "Drogon",
  surname: "",
  age: "3",
  eyeColor: "Greenish",
  dob:"2015-04-02T00:00:00.000Z",
  addres:"Sky",
  gender:"MALE",
  height:"over 9000",
  weight:1256,
  pressure:"Not yet mesured",
  bmi:"nan",
  weightChanges:{
    "2009":0,
    "2010":0,
    "2011":0,
    "2012":0,
    "2013":0,
    "2014":0,
    "2015":7,
    "2016":57,
    "2017":189,
    "2018":1134,
    "2019":1256,
  },
  alergies:{
    alergy1:"water",
    alergy4:"air",
    alergy5:"polen",
    alergy6:"icecream"
  },
  medicine:{
    med3:"Neoral",
  }
};
  ///////////////////////

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(curentUser) {
  ehrId = "";

  
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {
      "Authorization": getAuthorization()
    },
    success: function (data) {
      ehrId = data.ehrId;
      curentUser.ehrId = data.ehrId;
      var partyData = {
        firstNames: curentUser.name,
        lastNames: curentUser.surname,
        dateOfBirth: curentUser.dob,
        gender: curentUser.gender,
        additionalInfo: {"ehrId": ehrId}
      };
      console.log(partyData);
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        headers: {
          "Authorization": getAuthorization()
        },
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function (res) {
          if (res.action == 'CREATE') {
            console.log("Succesfully made an user!");
            document.getElementById("selectBody").style.display = "block";
            document.getElementById("patientInfo").style.display = "none";
          }
        },
        error: function(err) {
          console.log("Error message"  + JSON.parse(err.responseText).userMessage);
        }
      });
}
});

  return ehrId;
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
window.addEventListener('load', function () {



    document.getElementById("registerButton").addEventListener("click",function(){
      

    var newUser = emptyUser;

    newUser.name = document.getElementById("regName").value;
    newUser.surname = document.getElementById("regSurname").value;
    newUser.dob = document.getElementById("regDob").value;
    newUser.addres = document.getElementById("regAddress").value;
    newUser.gender = document.getElementById("regGender").value;

    document.getElementById("regName").value = "";
    document.getElementById("regSurname").value = "";
    document.getElementById("regDob").value = "";
    document.getElementById("regAddress").value = "";
    document.getElementById("regGender").value = "";
    document.getElementById("note").value = "";

    curentUser = newUser;
    $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        ehrId = data.ehrId;
        curentUser.ehrId = data.ehrId;
        var partyData = {
          firstNames: newUser.name,
          lastNames: newUser.surname,
          dateOfBirth: newUser.dob,
          gender: newUser.gender,
          additionalInfo: {"ehrId": ehrId}
        };
        console.log(partyData);
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (res) {
            if (res.action == 'CREATE') {
              $('html,body').animate({
                scrollTop: $(".topPage").offset().top},
                'slow');
              document.getElementById("selectBody").style.display = "none";
              document.getElementById("loginForm").style.display = "none";
              document.getElementById("regFalseAlert").style.display = "none";
              console.log("Succesfully mad an user:" + newUser.name + " " + newUser.surname);
              chartEdit(newUser);
              clearLists();
              showPatient(newUser);
              document.getElementById("regAlert").style.display = "block";
               // close the div in 2 secs
              window.setTimeout(closeDiv, 2000);
              function closeDiv()
              {
                  $("#regAlert").fadeOut("slow", null);
              }
            }
          },
          error: function(err) {
            console.log("Error message"  + JSON.parse(err.responseText).userMessage);
            document.getElementById("regFalseAlert").style.display = "block";
              
          }
        });
    }
    });



  });
  



  document.getElementById("loginButton").addEventListener("click",function(){



    var ehrId = $("#ehridLogIn").val();

    if (!ehrId || ehrId.trim().length == 0) {
      document.getElementById("logFalseAlert").style.display = "block";
    } else {
      $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {
          "Authorization": getAuthorization()
        },
        success: function (data) {
          var party = data.party;
          console.log(party);
          curentUser = emptyUser;
          curentUser.name = party.firstNames;
          curentUser.surname = party.lastNames;
          curentUser.gender = party.gender;
          curentUser.dob = party.dateOfBirth;

          chartEdit(curentUser);
          clearLists();
          showPatient(curentUser);
          document.getElementById("loginForm").style.display = "none";
        },
        error: function(err) {
          
            JSON.parse(err.responseText).userMessage + "'!";
            document.getElementById("logFalseAlert").style.display = "block";
        }
      });
    }



  });
    
  document.getElementById("generate").addEventListener("click",function(){

    generirajPodatke(Ragnar);
    generirajPodatke(Arya);
    generirajPodatke(Drogon);
    document.getElementById("loginForm").style.display = "none";
    //chartEdit(Ragnar);
    //clearLists();
    //showPatient(Ragnar);
 

  });

  var curentUser = Ragnar;
  
  document.getElementById("float").addEventListener("click",function() {
    $('html,body').animate({
        scrollTop: $(".register").offset().top},
        'slow');
  }); //// ANIMATION SCROOL BUTTON

  
  window.clearLists = function(){

    document.getElementById("alergiesList").innerHTML = "";
    document.getElementById("medsList").innerHTML = "";

  } //// CLEARING THE LISTS BEFORE FILL


  window.showPatient = function(patient){

    document.getElementById("profileName").innerHTML = patient.name + " " + patient.surname;
    document.getElementById("dob").innerHTML = patient.dob;
    document.getElementById("address").innerHTML = patient.addres;
    document.getElementById("gender").innerHTML = patient.gender;
    document.getElementById("age").innerHTML = patient.age + " years";
    document.getElementById("height").innerHTML = patient.height + " cm";
    document.getElementById("weight").innerHTML = patient.weight + " kg";
    document.getElementById("pressure").innerHTML = patient.pressure;
    document.getElementById("bmi").innerHTML = (patient.weight / ((patient.height/100) * (patient.height/100)));
    document.getElementById("eye").innerHTML = patient.eyeColor;
    document.getElementById("ehrid").innerHTML = patient.ehrId;
    
    for(var alergi in patient.alergies){
      
      var ul = document.getElementById("alergiesList");
      var li = document.createElement("li");
      li.appendChild(document.createTextNode(patient.alergies[alergi]));
      ul.appendChild(li);
    }

    for(var meds in patient.medicine){
       var ul = document.getElementById("medsList");
        var li = document.createElement("li");
        li.appendChild(document.createTextNode(patient.medicine[meds]));
        ul.appendChild(li);
    }

    document.getElementById("patientInfo").style.display = "block";

  }//// FILL THE LISTS &&& Making the graph


  var user = document.getElementById("Petients").value;
  var startingURL = "https://chart.googleapis.com/chart?cht=bvs&chs=400x300&chxt=x,y&chd=t:0";

  console.log(user);

  window.chartEdit = function(user){
    startingURL = "https://chart.googleapis.com/chart?cht=bvs&chs=450x300&chxt=x,y&chxr=0,2009,2019,1&chd=t:0";
    for(var weight in user.weightChanges){
      startingURL += ("," + user.weightChanges[weight]);

      console.log("loop");
    }
    document.getElementById("chart").src = startingURL;
  }
  

  window.selectPatient = function(patient){

    console.log(patient.value);

    switch (patient.value) {
      case "1":
        curentUser = Ragnar;
        chartEdit(Ragnar);
        clearLists();
        showPatient(Ragnar);
        break;
      case "2":
        curentUser = Arya;
        chartEdit(Arya);
        clearLists();
        showPatient(Arya);
        break;
      case "3":
        curentUser = Drogon;
        chartEdit(Drogon);
        clearLists();
        showPatient(Drogon);
        break;
      default:
        break;
    }


  

  }////ON SELECT PATIENT CHANGES

});